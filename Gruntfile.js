/*
 * Backgrid.js Totals Footer
 *
 * Copyright (c) 2014 Base79
 * Licensed under the MIT license.
 * https://bitbucket.org:base79/backgridjs-total-footer/LICENSE-MIT
 */


/*global module:false*/
module.exports = function (grunt) {

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            gruntfile: {
                src: 'Gruntfile.js'
            },
            files: {
                src: [
                    'src/**/*.js',
                    'test/**/*.js'
                ]
            }
        },
        qunit: {
            all: ['test/**/*.html']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-qunit');

    grunt.registerTask('default', ['jshint', 'qunit']);
};
