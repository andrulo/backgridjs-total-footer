/*
 * Backgrid.js Totals Footer
 *
 * Copyright (c) 2014 Base79
 * Licensed under the MIT license.
 * https://bitbucket.org:base79/backgridjs-total-footer/LICENSE-MIT
 */


/*global _, Backbone, Backgrid, QUnit */

QUnit.module('Test Totals Footer', {
    setup: function () {
        this.model = Backbone.Model.extend({});
        this.collection = Backbone.Collection.extend({model: this.model});

        // To prevent us calling grid.render() each time the collection models
        // change, simply render on collection sync/reset.
        // The grid is initially appended to options.$containerBox.
        this.grid = Backgrid.Grid.extend({
            initialize: function (options) {
                Backgrid.Grid.prototype.initialize.apply(this, arguments);
                options.$containerBox.append(this.el);
                this.listenTo(this.collection, 'sync reset', function () {
                    this.render();
                }, this);
            }
        });

        this.apiData = [{
            id: 1,
            name: 'England',
            matchesPlayed: 3,
            scored: 2,
            foulsCommitted: 37,
            yellowCards: 4,
            redCards: 0,
            lastGame: '2014-06-24'
        }, {
            id: 2,
            name: 'Germany',
            matchesPlayed: 7,
            scored: 18,
            foulsCommitted: 91,
            yellowCards: 6,
            redCards: 0,
            lastGame: '2014-07-13'
        }, {
            id: 3,
            name: 'Brazil',
            matchesPlayed: 7,
            scored: 11,
            foulsCommitted: 123,
            yellowCards: 14,
            redCards: 0,
            lastGame: '2014-07-12'
        }];

        this.collectionInstance = new this.collection();

        this.triggerRender = function () {
            this.collectionInstance.reset(this.apiData, {parse: true});
        };

        var element = document.createElement('div');
        $(element).addClass('grid-area');
        $('body').append(element);
    },
    teardown: function () {
        $('.grid-area').remove();
    }
});

QUnit.test('Test triggerRender', function (assert) {
    this.triggerRender();
    assert.equal(this.collectionInstance.length, 3);
});

QUnit.test('Test default Footer', function (assert) {
    var columns = [{
        name: 'id',
        label: 'ID',
        cell: 'number'
    }, {
        name: 'name',
        label: 'Name',
        cell: 'string'
    }, {
        name: 'matchesPlayed',
        label: 'Played',
        cell: 'number'
    }, {
        name: 'scored',
        label: 'Goals Scored',
        cell: 'number'
    }, {
        name: 'lastGame',
        label: 'Last Game',
        cell: 'date'
    }];

    var gridInstance = new this.grid({
        columns: columns,
        collection: this.collectionInstance,
        footer: Backgrid.Extension.TotalFooter.Footer,
        $containerBox: $('.grid-area')
    });

    this.triggerRender();

    var $footer = $('.grid-area .backgrid tfoot');

    var expected = '<tr><td class="number-cell id">6.00</td><td class="string-cell name"></td><td class="number-cell matchesPlayed">17.00</td><td class="number-cell scored">31.00</td><td class="date-cell lastGame"></td></tr>';

    assert.equal($footer.html(), expected);
});

QUnit.test('Test Footer with custom first cell', function (assert) {
    var columns = [{
        name: 'id',
        label: 'ID',
        cell: 'number'
    }, {
        name: 'name',
        label: 'Name',
        cell: 'string'
    }, {
        name: 'matchesPlayed',
        label: 'Played',
        cell: 'number'
    }, {
        name: 'scored',
        label: 'Goals Scored',
        cell: 'number'
    }, {
        name: 'lastGame',
        label: 'Last Game',
        cell: 'date'
    }];

    var gridInstance = new this.grid({
        columns: columns,
        collection: this.collectionInstance,
        footer: Backgrid.Extension.TotalFooter.Footer.extend({
            firstColumnValue: 'Total'
        }),
        $containerBox: $('.grid-area')
    });

    this.triggerRender();

    var $footer = $('.grid-area .backgrid tfoot');

    var expected = '<tr><td class="number-cell id">Total</td><td class="string-cell name"></td><td class="number-cell matchesPlayed">17.00</td><td class="number-cell scored">31.00</td><td class="date-cell lastGame"></td></tr>';

    assert.equal($footer.html(), expected);
});

QUnit.test('Test custom Footer Cell', function (assert) {
    var columns = [{
        name: 'id',
        label: 'ID',
        cell: 'number'
    }, {
        name: 'name',
        label: 'Name',
        cell: 'string'
    }, {
        name: 'yellowCards',
        label: 'Number of Yellows',
        cell: 'number',
        footerCell: Backgrid.Extension.TotalFooter.Cell.extend({
            getValue: function () {
                // return the average number.
                return this.formatter.fromRaw(this._sumColumn() / 3);
            }
        })
    }];

    var gridInstance = new this.grid({
        columns: columns,
        collection: this.collectionInstance,
        footer: Backgrid.Extension.TotalFooter.Footer,
        $containerBox: $('.grid-area')
    });

    this.triggerRender();

    var $footer = $('.grid-area .backgrid tfoot');

    var expected = '<tr><td class="number-cell id">6.00</td><td class="string-cell name"></td><td class="number-cell yellowCards">8.00</td></tr>';

    assert.equal($footer.html(), expected);
});

QUnit.test('Test custom Cell formatter', function (assert) {
    var customFormatter = _.extend({}, Backgrid.CellFormatter.prototype, {
        fromRaw: function (rawValue, model) {
            return 'ID: ' + rawValue;
        }
    });

    var columns = [{
        name: 'id',
        label: 'ID',
        cell: Backgrid.NumberCell.extend({
            formatter: customFormatter
        })
    }, {
        name: 'name',
        label: 'Name',
        cell: 'string'
    }, {
        name: 'yellowCards',
        label: 'Number of Yellows',
        cell: 'number'
    }];

    var gridInstance = new this.grid({
        columns: columns,
        collection: this.collectionInstance,
        footer: Backgrid.Extension.TotalFooter.Footer,
        $containerBox: $('.grid-area')
    });

    this.triggerRender();

    var $footer = $('.grid-area .backgrid tfoot');

    var expected = '<tr><td class="number-cell id">ID: 6</td><td class="string-cell name"></td><td class="number-cell yellowCards">24.00</td></tr>';

    assert.equal($footer.html(), expected);
});
